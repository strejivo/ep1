#include <cstdlib>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct normalLine{
    normalLine(int x1, int y1, int x2, int y2){
        int gcd = __gcd(x1-x2, y2-y1);
        x = (y2 - y1)/gcd;
        y = (x1 - x2)/gcd;
        c = -(x1 * x + y1*y);
    }
    long x;
    long y;
    long c;
};

struct parametricLine{
    parametricLine(int x1, int y1, int x2, int y2){
        int gcd = __gcd(x1-x2, y1-y2);
        xConst = x1;
        yConst = y1;
        xCoef = (x1-x2)/gcd;
        yCoef = (y1-y2)/gcd;
    }
    long xConst;
    long yConst;
    long xCoef;
    long yCoef;
};

bool intersects(parametricLine a, normalLine b){
    double eps;
    long c = (b.x*a.xCoef + b.y*a.yCoef);
    long d = (-b.c - b.y*a.yConst - b.x*a.xConst);
    if (c == 0){
        return (d == 0);
    }
    else {
        eps = 1e-8;
        double t = d/c; 
        if (t - eps <= 1 && t + eps  >= 0 ) return true;
        else return false;
    }
}

bool intersects(parametricLine a, normalLine b, double & x, double & y){
    double eps;
    long c = (b.x*a.xCoef + b.y*a.yCoef);
    long d = (-b.c - b.y*a.yConst - b.x*a.xConst);
    eps = 1e-8;
    double t = d/c; 
    if (t - eps <= 1 && t + eps  >= 0 ) {
        x = a.xConst + a.xCoef*t;
        y = a.yConst + a.yCoef*t;
        return true;
    }
    else return false;
}

long getDistance(normalLine a, double x, double y){
    return fabs(a.x*x + a.y*y +a.c)/sqrt(a.x*a.x + a.y*a.y);
}

int main(int argc, char** argv) {
    int cnt;
    while(cin >> cnt && cnt != 0){
        vector<pair<int, int > > pipes;
        for (int i = 0; i < cnt; i++){
            int x, y;
            cin >> x >> y;
            pipes.push_back(make_pair(x, y));
        }
    }
    return 0;
}