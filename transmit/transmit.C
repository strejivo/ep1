#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

#define eps 1e-8

using namespace std;

bool isInReach(int x, int y, double r){
	return ((pow(x,2) + pow(y,2)) < (pow(r,2)+eps*r));
}

int main (void){
	int x, y;
	double radius;
	while(cin >> x >> y >> radius){
		if (radius < 0) return 0;
		int counter = 0;
		int cnt;
		cin >> cnt;
		vector<double> angles;
		for ( int i = 0; i < cnt; i++){
			int x1, y1;
			cin >> x1 >> y1;
			if ( x1 == x && y1 == y) {
				counter++;
				continue;
			}
			if (!isInReach(x1-x, y1-y, radius)) continue;
			angles.push_back(atan2(y1-y, x1-x));
		}
		if (angles.size() <= 2 ) {
			cout<<angles.size()+counter<<endl;
			continue;
		}
		sort(angles.begin(), angles.end());
		int tmp = angles.size();
		for (int i = 0; i < tmp; i++){
			angles.push_back(angles[i] + 2*M_PI);
		}
		int ret = 0;
		int it = 0;
		bool stop = false;
		for ( int i = 0; i < angles.size() && !stop; i++){
			while(angles[i] + M_PI + eps > angles[it]) {
				if (it+1 > angles.size()){ stop = true; break;}
				it++;
			}
			ret = max(ret, it-i);
		}
		cout<<ret+counter<<endl;
	}
	return 0;
}
