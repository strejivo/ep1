#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lock.h"

#define advanced 1
#define basic 0

void resize(char ** arr, int * size){
	*arr = (char *) realloc(*arr, *size*2);
	*size *= 2;
}


int compare( const void *a, const void *b) {
	return *(char*)a - *(char*)b;
}

int cnt;
int mode;
int permute(char ** symbols, int size, char ** whole, int wanted){
	printf("%d\n", strlen(*whole));
	if (strlen(*whole) == wanted){
            cnt++;
            if (mode == basic){
                printf("%s\n", *whole);
                return 0;
            }
            else if(unlock(*whole)){
                printf("%s - %dx\n", *whole, cnt);
                return 1;
            }
	}
	else{
            for (int i = 0; i < size; i++){
                char tmp = (*symbols)[i];
                for (int j = i; j > 0; j--){
                    (*symbols)[j] = (*symbols)[j-1];
                }
                (*symbols)[0] = tmp;
                char * newBegin = ((*symbols)+1);
                if (permute(&newBegin, size-1, whole, wanted) == 1) return 1;
                for (int j = 0; j < i; j++){
                    (*symbols)[j] = (*symbols)[j+1];
                }
                (*symbols)[i] = tmp;
            }
            return 0;
	}
        return 0;
}

void printError(char ** s, const char * err){
    free(*s);
    fprintf(stderr, "%s\n", err);
}

int main (int argc, char *argv[]){
    cnt = 0;
    mode = basic;
    if (argc > 1 && strcmp(argv[1], "-prp-optional")==0){
    	mode = advanced;
    }
    int maxSize = 2;
    int size = 0;
    char * s = (char *) malloc(maxSize*sizeof(int));
    char c = 0;
    while(scanf("%c", &c) && c!= '\n' && !feof(stdin)){
        if (!(c >= 'a' && c <='z') && !(c>= 'A' && c <= 'Z') && !(c >= '0' && c <= '9')){
            printError(&s, "Error: Chybny vstup!");
            return 100;
        }
            if (size+1==maxSize) resize(&s, &maxSize);
            s[size++] = c;
    }
    if (size == 0){
        printError(&s, "Error: Chybny vstup!");
        return 100;
    }
    s[size] = '\0';
    //qsort(s, size, sizeof(char), compare);
    if ( (mode == advanced) & (permute(&s, size, &s, 4) != 1)) {
        printError(&s, "Error: Heslo nebylo nalezeno!");
        return 101;
    }
    free(s);
    return 0;
}
